//
//  PrincipalAppDelegate.h
//  ControleMemoria
//
//  Created by Bruno Hassuna on 2/2/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrincipalAppDelegate : UIResponder <UIApplicationDelegate>{
    
    NSArray * lista1, * lista2;
}

@property (strong, nonatomic) UIWindow *window;

@end
