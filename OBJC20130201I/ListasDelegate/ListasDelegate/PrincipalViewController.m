//
//  PrincipalViewController.m
//  ListasDelegate
//
//  Created by Bruno Hassuna on 2/3/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import "PrincipalViewController.h"

@interface PrincipalViewController ()

@end

@implementation PrincipalViewController

-(void)ler:(id)sender{
    for (int i=0; i<lista.count; i++) {
        //pego um dos elementos da lista
        NSDictionary * d = [lista objectAtIndex:i];
        
        NSLog(@"Nome: %@ Email:%@ Tel:%@",[d objectForKey:@"nome"],[d objectForKey:@"mail"],[d objectForKey:@"phone"]);
    }
    
    for (NSDictionary * d in lista) {
        NSLog(@"Nome: %@ Email:%@ Tel:%@",[d objectForKey:@"nome"],[d objectForKey:@"mail"],[d objectForKey:@"phone"]);
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [UIView animateWithDuration:0.4 animations:^{
        textField.alpha = 1;
    }];
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    [UIView animateWithDuration:0.4 animations:^{
        textField.alpha = 0.3;
    }];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    //usando o textField posso comparar com meus IBOutlets e saber qual das caixas de texto gerou o evento
    if (textField == txtNome) {
        //passo o foco para o email
        [txtEmail becomeFirstResponder];
    }
    if (textField == txtEmail) {
        [txtTelefone becomeFirstResponder];
    }
    if (textField == txtTelefone) {
        //fecho o teclado se for a ultima caixa de texto
        [txtTelefone resignFirstResponder];
    }
    return true;
}
-(void)salvar:(id)sender{
    //crio um dicionario que empacota os dados de uma pessoa
    NSDictionary * pessoa = [NSDictionary dictionaryWithObjectsAndKeys:txtTelefone.text,@"phone",
                             txtNome.text,@"nome",
                             txtEmail.text,@"mail", nil];
    //salvo esses dados na lista
    [lista  addObject:pessoa];
    
    //salvo no aparelho
    [[NSUserDefaults standardUserDefaults]setObject:lista forKey:@"minhaLista"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //busco no aparelho os dados que o usuario já salvou!
    lista = [[NSUserDefaults standardUserDefaults]objectForKey:@"minhaLista"];
    
    if (lista==nil) {
        //se não existia lista nenhuma crio a primeira lista
        lista = [[NSMutableArray alloc]init];
    }else{
        //converto para lista mutavel
        lista = [NSMutableArray arrayWithArray:lista];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
