//
//  PrincipalViewController.m
//  Botoes
//
//  Created by Bruno Hassuna on 2/2/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import "PrincipalViewController.h"

@interface PrincipalViewController ()

@end

@implementation PrincipalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeInfoDark];
    btn.frame = CGRectMake(50, 100, 50, 50);
    [self.view addSubview:btn];
    
    
    //defino que o botao chamara o metodo clique botao quando for pressionado
    [btn addTarget:self action:@selector(cliqueBotao) forControlEvents:UIControlEventTouchUpInside];
}
-(void)cliqueBotao{
    self.view.backgroundColor = [UIColor redColor];
    
    //quando coloco : na frente do @selector eu estou mandando o Timer que gerou o evento como parametro
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(ping:) userInfo:nil repeats:true];
}
int x=3;
-(void)ping:(NSTimer*)abacate{
    x--;
    NSLog(@"Estou no %d",x);
    if (x<0) {
        [abacate invalidate];
        self.view.backgroundColor = [UIColor whiteColor];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
