//
//  PrincipalViewController.h
//  ListasEDicionarios
//
//  Created by Bruno Hassuna on 2/3/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImagemArmazenator.h"

@interface PrincipalViewController : UIViewController
{
    ImagemArmazenator * meuCache;
    NSString * txt;
}

@property IBOutlet UIImageView * imgBack;
@property IBOutlet UITextField * txtURL;
@property IBOutlet UILabel * lblInfo;


-(IBAction)buscar:(id)sender;

@end
