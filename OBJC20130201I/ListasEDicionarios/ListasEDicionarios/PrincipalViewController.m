//
//  PrincipalViewController.m
//  ListasEDicionarios
//
//  Created by Bruno Hassuna on 2/3/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import "PrincipalViewController.h"

@interface PrincipalViewController ()

@end

@implementation PrincipalViewController

-(void)buscar:(id)sender{
    self.lblInfo.text = @"CARREGANDO...";
    //farei um processo lento, não posso fazer na thread principal, então crio uma thread secundaria
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [self performSelectorOnMainThread:@selector(leTXT) withObject:nil waitUntilDone:TRUE];
        
        UIImage * img = [meuCache getImageByURL:txt];
        //não posso "mexer" na tela a partir de uma thread secundaria, volto para a thread principal para atualizar a tela
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imgBack.image = img;
            self.lblInfo.text = @"";
        });
        
    });
    //fecho o teclado
    [self.txtURL resignFirstResponder];
}
-(void)leTXT{
    txt = self.txtURL.text;
}

-(void)buscarV2:(id)sender{
    self.lblInfo.text = @"CARREGANDO...";
    //farei um processo lento, não posso fazer na thread principal, então crio uma thread secundaria
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
       __block NSString * txt=nil;
       dispatch_async(dispatch_get_main_queue(), ^{
           txt = self.txtURL.text;
       });
        
        UIImage * img = [meuCache getImageByURL:txt];
        //não posso "mexer" na tela a partir de uma thread secundaria, volto para a thread principal para atualizar a tela
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imgBack.image = img;
            self.lblInfo.text = @"";
        });

    });

    //fecho o teclado 
    [self.txtURL resignFirstResponder];
}

-(void)buscarV1:(id)sender{
    self.lblInfo.text = @"CARREGANDO...";
    //farei um processo lento, não posso fazer na thread principal, então crio uma thread secundaria
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        
        UIImage * img = [meuCache getImageByURL:self.txtURL.text];
        //não posso "mexer" na tela a partir de uma thread secundaria, volto para a thread principal para atualizar a tela
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imgBack.image = img;
            self.lblInfo.text = @"";
            
        });
        
    });
    
    
    //fecho o teclado
    [self.txtURL resignFirstResponder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    meuCache =[[ImagemArmazenator alloc]init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
