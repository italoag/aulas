//
//  PrincipalViewController.m
//  Thread2
//
//  Created by Bruno Hassuna on 2/2/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import "PrincipalViewController.h"

@interface PrincipalViewController ()

@end

@implementation PrincipalViewController

-(void)start:(UIButton *)sender{
    sender.hidden = true;
    
    //vou disparar um processo numa "thread" secundaria, para liberar a thread principal
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //os comandos que eu rodo dentro desse bloco rodarão numa thread secundaria
        for (int i=0; i<9; i++) {
            NSString * urlFoto = [NSString stringWithFormat:@"http://brunoeiti.com/jessica/jessica%d.jpg",i];
            NSURL * url =[NSURL URLWithString:urlFoto];
            //baixo o conteudo da imagem
            NSData * dados = [NSData dataWithContentsOfURL:url];
            
            //crio um objeto visual para apresentar essa imagem
            UIImageView * imgView = [[UIImageView alloc]init];
            imgView.frame = CGRectMake((i%3)*106, (i/3)*153, 106, 153);
            imgView.image = [UIImage imageWithData:dados];
            
            //coloco a imagem na tela
            dispatch_async(dispatch_get_main_queue(), ^{
               //os comandos que eu rodar aqui dentro rodam na thread principal, assim eu posso mexer na tela
                imgView.alpha = 0;
                CGRect posicaoReal = imgView.frame;
                imgView.frame = CGRectMake(0, 0, 1, 1);
                [self.view addSubview:imgView];
                [UIView animateWithDuration:0.6 animations:^{
                    imgView.alpha = 1;
                    imgView.frame = posicaoReal;
                }];

            });
        }
    });
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
